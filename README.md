## Getting Started ##
---------------

To get started with OMNI sources to build TWRP, you'll need to get
familiar with [Git and Repo](https://source.android.com/source/using-repo.html).
    
To initialize a shallow clone, which will save even more space, use a command like this:

    repo init --depth=1 -u https://gitlab.com/newinvertifyrecovery/manifest.git -b master

Then to sync up:

    repo sync

Then to build:

     cd <source-dir>; export ALLOW_MISSING_DEPENDENCIES=true; . build/envsetup.sh; lunch omni_<device>-eng; mka recoveryimage


BUILD NOTES

If you device have notch screen- you need use notch theme. Edit BoardConfig.mk in device tree and set
TW_THEME := notch_hdpi

And you need add patchs in sbin

    cd device/*vendor*/*devicecodename*
    git remote add midotree https://gitlab.com/newinvertifyrecovery/device/mido.git
    git fetch midotree

* This add magisk manager recovery script in sbin - you can call "mm" in terminal for use
    git cherry-pick cf6cb8420536a06f5cb8da8913a3d58eae232ab6
*This add 5 generic patchs*
    git cherry-pick da4435bffaecf903294b67301eb90033ca03c778
*This add delete fingerprints patch (Working on mido and some other xiaomi devices, if you dont use- test before adding or search patch for you device)
    git cherry-pick 9a5ce3d737ed33c0b9e6699f64fb0037a9a76508

Last patch- Splash.img change. Search splash change zip for you device, and add it to recovery/root/sbin with name defsplash.zip. Or edit portrait.xml and add another patch


